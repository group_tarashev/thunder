import moment from 'moment-timezone'
import { useEffect, useState } from 'react'

type ClockProp = {
  location: {
    tz_id?: string | null
  } | null
}

const Clock = ({location}:ClockProp) => {
    const[time, setTime] = useState('')
    useEffect(()=>{
        const int = setInterval(()=>{
          if(location?.tz_id){
            const day = moment.tz(location.tz_id)
            const shema = day.format("DD-MM-YYYY HH:mm:ss")
            setTime(shema);
          }
        },1000)
        return () => clearInterval(int);
    },[location?.tz_id])
  return (
    <div>{time}</div>
  )
}

export default Clock