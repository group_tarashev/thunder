import moment from "moment";

type obj = {
  numbOfcom: number | null;
  months: Array<string>;
  getDateMonth: any;
  weekdayNames: Array<string>;
  numberOfweek: number | null;
  numberOfday: number | null;
  substrDate: string;
};

export const calcDate = (date : string) => {
  const obj : obj = {
    numbOfcom: null,
    months: [],
    getDateMonth: null,
    weekdayNames: [],
    numberOfweek: null,
    numberOfday: null,
    substrDate: "",
  };
  let substrDate = date.substring(8, 10);
  let stringOfday = moment(new Date()).format("DD");
  obj.weekdayNames  = ["Thu", "Fri", "Sat", "Sun", "Mon", "Tue", "Wed"];
  obj.numbOfcom = parseInt(substrDate);
  obj.numberOfweek = parseInt(stringOfday);
  obj.months = [
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "Jun",
    "Jul",
    "Aug",
    "Sep",
    "Oct",
    "Nov",
    "Dec",
  ];
  obj.getDateMonth = date.substring(5, 7);
  const numDateMont = typeof obj.getDateMonth === 'string' ?  parseInt(obj.getDateMonth) : obj.getDateMonth;
  obj.getDateMonth = numDateMont;
  if (obj.numbOfcom > obj.weekdayNames.length - 1) {
    obj.numbOfcom -= obj.weekdayNames.length;
  }
  obj.substrDate = substrDate;

  return obj;
};
