import { useEffect, useState } from "react";
import "./App.css";
import { getCity } from "./api/city";
import { getWeath } from "./api/weather";
import Main from "./Header";

function App() {
  const [city, setCity] = useState("");
  const [search, setSearch] = useState<string>("");
  const [handle, setHandle] = useState(false);
  const [data, setData] = useState({
    current: null,
    location: null,
    forest: null,
  });

  useEffect(() => {
    if (city && search) getWeath(setData, search);
  }, [city]);
  useEffect(() => {
    if (city && search) getWeath(setData, search);
  }, [handle]);
  useEffect(() => {
    const fetch = async () => {
      try {
        await getCity(setCity, setSearch);
      } catch (error) {
        console.log(error);
      }
    };
    fetch();
  }, []);

  return (
    <div>
      <Main
        current={data?.current}
        location={data?.location}
        forest={data?.forest}
        setSearch={setSearch}
        search={search}
        handle={handle}
        setHandle={setHandle}
        city={city}
      />
    </div>
  );
}

export default App;
