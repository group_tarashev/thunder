import  {  useState } from "react";
import "./styles/main.css";
import Search from "./components/Search";
import LeftSide from "./components/LeftSide";
import RightSide from "./components/RightSide";
import "./styles/main.css";
export type Main = {
  forest: { forecastday: [] } | null;
  location: {
    name: string;
    tz_id: string | null;
  } | null;
  current: { condition: { icon: string; text: string }; temp_c: number } | null;
  search: string;
  setSearch: (search: string) => void;
  handle: boolean;
  setHandle: (handle: boolean) => void;
  city: string;
};

const Main = ({
  forest,
  location,
  current,
  search,
  setSearch,
  handle,
  setHandle,
  city,
}: Main) => {
  const [show, setShow] = useState<boolean>(false);

  return (
    <div className="main">
      <Search
        search={search}
        setSearch={setSearch}
        handle={handle}
        setHandle={setHandle}
        show={show}
        setShow={setShow}
      />
      <LeftSide
        show={show}
        setShow={setShow}
        current={current}
        location={location}
        city={city}
        setSearch={setSearch}
        handle={handle}
        setHandle={setHandle}
      />
      <RightSide forest={forest} current={current} />
    </div>
  );
};

export default Main;
