import "../styles/card.css";
import { calcDate } from "../tools/filter/filter";
export type CardProps ={
  day: {
    mintemp_c: number,
    mintemp_f: number,
    maxtemp_c: number,
    maxtemp_f: number,
    condition: {
      icon: string
    }
  },
  date: string,
  faren: boolean
}
const Card = ({day, date, faren } : CardProps) => {
  
  const obj = calcDate(date)
  const {numberOfweek, numbOfcom, weekdayNames, substrDate, getDateMonth, months} = obj;
  if(numbOfcom && numberOfweek && getDateMonth)
  return (
    <div className="card">
      {numberOfweek + 1 === parseInt(substrDate) && <h5 className="card-date"> Tommorow</h5>}
      {numberOfweek + 1  !== parseInt(substrDate)  && (
        <div>
          <span>{weekdayNames[numbOfcom]},  </span>
          <span>{substrDate} </span>
          <span>{months[getDateMonth  - 1]}</span>
        </div>
      )}
      <img src={day?.condition?.icon} alt="" className="card-img" />
      <div className="max-min-deg">
        {faren || <span className="hight"> {day?.maxtemp_c}℃</span>}
        {faren && <span className="hight"> {day?.maxtemp_f}℉</span>}
        {faren || <span>{day?.mintemp_c}℃</span>}
        {faren && <span>{day?.mintemp_f}℉</span>}
      </div>
    </div>
  );
};

export default Card;
