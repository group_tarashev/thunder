import "../styles/hightlightcard.css";
import { BsCursorFill } from "react-icons/bs";
import { ConditionProp } from "./RightSide";

const WindCard = ({ current }: ConditionProp) => {
  if(current?.wind_degree)
  return (
    <div className="hight-card">
      <h4 className="hight-title">Wind status</h4>
      <div className="hight-values">
        <h1>{current?.wind_kph}</h1>
        <h1 className="mph">kmh</h1>
      </div>
      <div className="hight-tools">
        <BsCursorFill
          className="point-wind"
          style={{ transform: `rotate(${current?.wind_degree - 45}deg)` }}
        />
        <h6>{current?.wind_dir}</h6>
      </div>
    </div>
  );
};

export default WindCard;
