import { BiCurrentLocation } from "react-icons/bi";
import { HiLocationMarker } from "react-icons/hi";
import { WiDegrees } from "react-icons/wi";
import Clock from "../tools/clock/Clock";
import '../styles/left-side.css'
type LeftProps = {
  show: boolean;
  setShow: (show: boolean) => void;
  current:
    | { condition: { icon: string; text: string }; temp_c: number }
    | null;
  location: { name: string, tz_id: string | null} | null;
  city: string;
  setSearch: (prop: string) => void;
  setHandle: (handle: boolean) => void;
  handle: boolean;
};

const LeftSide = ({
  show,
  setShow,
  current,
  location,
  city,
  setSearch,
  setHandle,
  handle,
}: LeftProps) => {
  return (
    <div className="left-side">
      <div className="left-top">
        <button
          className="btn-search"
          onClick={() => {
            setShow(!show);
          }}
        >
          Search for places
        </button>
        <BiCurrentLocation
          size={40}
          onClick={() => {
            setSearch(city);
            setHandle(!handle);
          }}
          className={
            city?.toLowerCase() === location?.name.toLowerCase()
              ? "location-curr"
              : "location"
          }
        />
      </div>
      <div className="left-middle">
        <img src={current?.condition?.icon} alt="" className="img-condition" />
        <div className="current-temp">
          <h1 className="deg-numb">{current?.temp_c}</h1>
          <div className="left-meash">
            <WiDegrees className="degree" />
            <h1 className="deg-lett">C</h1>
          </div>
        </div>
      </div>
      <div className="left-bottom">
        <div className="condition">
          <h1>{current?.condition?.text}</h1>
        </div>
        <div className="today">
          <h6>Today </h6>
          <h6>{<Clock location={location} />}</h6>
        </div>
        <div className="place">
          <HiLocationMarker />
          <h6>{location?.name}</h6>
        </div>
      </div>
    </div>
  );
};

export const api = "d5ce2f9e42bf4b109f2180830230304";
export default LeftSide;
