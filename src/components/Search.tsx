import React, { useState } from "react";
import { BsSearch } from "react-icons/bs";
import { AiOutlineClose } from "react-icons/ai";
import "../styles/search.css";

type SearchProp = {
  search: string;
  setSearch: (search: string) => void;
  handle: boolean;
  setHandle: (handle: boolean) => void;
  setShow: (show: boolean) => void;
  show: boolean;
};

const Search = ({
  setShow,
  search,
  setSearch,
  handle,
  setHandle,
  show,
}: SearchProp) => {
  const storedList = localStorage.getItem("list");
  const parsList = storedList ? JSON.parse(storedList) : [];
  const [btns, setBtns] = useState({
    isActive: 0,
    btn: parsList,
  });
  const active = (i: number) => {
    setBtns({ ...btns, isActive: btns.btn[i] });
  };
  const classActive = (i: number) => {
    if (btns.isActive === btns.btn[i]) {
      return "suggest-li isA";
    } else {
      return "suggest-li";
    }
  };
  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    e.preventDefault();
    setSearch(e.target.value);
    const searchId = document.getElementById("search") as HTMLInputElement | null
    if(searchId){
      searchId.value = "newValue";
    }
  };
  const keyDown = (e: any) => {
    if (e.key === "Enter") {
      setHandle(!handle);
      // setShow(false);
    }
  };
  let list : [{name: string}]  = parsList;
  const store = (e: any) => {
    e.preventDefault();
    if (
      !list.some(
        (item: { name: string }) =>
          item.name.toLowerCase() === search.toLowerCase()
      )
    ) {
      list.push({ name: search });
      localStorage.setItem("list", JSON.stringify(list));
    }
  };
  const handleSelect = (i: number) => {
    const inner = document.querySelectorAll(".suggest-li");
    const select = inner[i] as HTMLElement | null;
    if(select){
      setSearch(select.innerText);
    }
    active(i);
    // setShow(false);
    setHandle(!handle);
  };
  return (
    <div className={show ? "search-active" : "search"}>
      <div className="search-close">
        <AiOutlineClose
          onClick={() => setShow(false)}
          style={{ cursor: "pointer" }}
        />
      </div>
      <div className="search-input-items">
        <form action="" onSubmit={store}>
          <input
            className="search-input"
            type="text"
            value={search}
            placeholder="Search locaiton"
            onChange={handleChange}
            id="search"
            onKeyDown={keyDown}
          />
          <BsSearch className="search-icon" />
          <button
            className="btn-search-input"
            onClick={() => {
              setHandle(!handle);
              // setShow(false);
            }}
          >
            Search
          </button>
        </form>
        <ul className={list?.length ? "suggest" : " suggest-null"}>
          {list?.map((item: { name: string }, i: number) => {
            return (
              <li
                key={i}
                className={item.name ? classActive(i) : "  "}
                onClick={() => handleSelect(i)}
              >
                {item.name}
              </li>
            );
          })}
        </ul>
      </div>
    </div>
  );
};

export default Search;
