import {  useState } from 'react'
import { WiDegrees } from "react-icons/wi";
import Card, { CardProps } from "./Card";
import WindCard from "./WindCard";
import Humidity from "./Hummidity";
import Visability from "./Visability";
import Presure from "./Presure";
import '../styles/right-side.css'
export type ConditionProp = {
  current:
    | {
        condition?: { icon?: string; text?: string };
        temp_c?: number;
        wind_kph?: number;
        wind_dir?: number;
        wind_degree?: number;
        humidity?: number,
        vis_km?: number,
        pressure_mb?: number
      }
    | null;
};
type RightProps ={
  forest: {forecastday: []} | null,
  current:
  | { condition: { icon?: string | undefined; text?: string | undefined }; temp_c?: number}
  | null;
}
const RightSide = ({forest, current} :RightProps) => {
    const [faren, setFaren] = useState(false);
  const fore = forest?.forecastday?.slice(1);
  return (
    <div className="right-side">
      <img className='bck-image' src="./pexels.webp" alt="" />
        <div className="change-degree">
          {/* CHANGE DEGREE */}
          <div
            className={faren ? "temperature " : "temperature temp-active"}
            onClick={() => {
              setFaren(false);
            }}
          >
            <WiDegrees className="d-deg" />
            <h1>C</h1>
          </div>
          <div
            className={faren ? "temperature temp-active" : "temperature"}
            onClick={() => {
              setFaren(true);
            }}
          >
            <WiDegrees className="d-deg" />
            <h1>F</h1>
          </div>
        </div>
        <div className="cards">
          {fore?.map((item :CardProps) => {
            return (
              <Card
                key={item.date}
                day={item.day}
                date={item.date}
                faren={faren}
              />
            );
          })}
        </div>
        <div className="hightlight">
          <h1>Today`s Hightlight</h1>
        </div>
        <div className="hightlight-cards">
          <WindCard current={current} />
          <Humidity current={current} />
          <Visability current={current} />
          <Presure current={current} />
        </div>
      </div>
  )
}

export default RightSide