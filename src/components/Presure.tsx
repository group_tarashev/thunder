import { ConditionProp } from './RightSide'

const Presure = ({current} :ConditionProp) => {
    // const {pressure_mb} = data.current
  return (
    <div className='hight-card'>
        <h4 className='hight-title'>Air Pressure</h4>
        <div className='hight-values'>
            <h1>{current?.pressure_mb}</h1>
            <h1 className='mph'>mb</h1>
        </div>
    </div>
  )
}

export default Presure