import '../styles/humidity.css'
import { ConditionProp } from './RightSide'

const Humidity = ({current} : ConditionProp) => {
    // const {humidity} = data.current
    if(current?.humidity)
  return (
    <div className='hight-card'>
        <h4 className='hight-title'>Humidity</h4>
        <div className='hight-values'>
            <h1>{current?.humidity}</h1>
            <h1 className='mph'>%</h1>
        </div>
        <div className='hight-bar'>
            <div className='pers'>
                <span>0</span>
                <span>50</span>
                <span>100</span>
            </div>
            <div className='pro-bar'></div>
            <div className='pro-pro' style={{width:`${ current?.humidity - (current?.humidity * 0.1)}%`}}></div>
            <div className='symb'>
                <span >%</span>
            </div>
        </div>
    </div>
  )
}

export default Humidity