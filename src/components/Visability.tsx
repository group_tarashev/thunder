import { ConditionProp } from './RightSide'

const Visability = ({current} : ConditionProp) => {
    // const {vis_km} = data.current
  return (
    <div className='hight-card'>
        <h4 className='hight-title'>Visability</h4>
        <div className='hight-values'>
            <h1>{current?.vis_km}</h1>
            <h1 className='mph'>kmh</h1>
        </div>
    </div>
  )
}

export default Visability