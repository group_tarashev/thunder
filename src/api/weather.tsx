import axios from "axios";
import { api } from "../components/LeftSide";
import React, { Dispatch } from "react";
// const apii = process.env.REACT_APP_API_KEY;

export const getWeath = async (
  setData: React.Dispatch<
    React.SetStateAction<{
      current: null;
      location: null;
      forest: null;
    }>
  >,
  search: string
) => {
  await axios
    .get(
      `//api.weatherapi.com/v1/forecast.json?key=${api}&q=${search}&days=6&aqi=no&alerts=no`
    )
    .then((res) => {
      setData((prev) => ({
        ...prev,
        current: res.data.current,
        location: res.data.location,
        forest: res.data.forecast.forecastday,
      }));
    })
    .catch((err) => {
      console.log(err);
    });
};
