import axios from "axios";
import { Dispatch } from "react";

export const getCity = async (setCity: React.SetStateAction<Dispatch<string>>, setSearch: React.SetStateAction<Dispatch<string>>) =>{
    axios.get("https://geolocation-db.com/json/").then((res) => {
      setCity(res.data.city);
      setSearch(res.data.city)
      return res.data.city
    }).catch(err => console.log(err));
}